package com.bubnii.controller;

import com.bubnii.model.task1.PingPong;
import com.bubnii.model.task2.FibonacciNumbers;
import com.bubnii.model.task3.FibonacciExecutors;
import com.bubnii.model.task4.FibonacciCallable;
import com.bubnii.model.task5.SchedulerExample;
import com.bubnii.model.task6.SynchronizeTest;
import com.bubnii.model.task7.PipeToCommunicate;

public class Controller {

    private PingPong pingPong;
    private FibonacciExecutors fibonacciExecutors;
    private FibonacciNumbers fibonacciNumbers;
    private FibonacciCallable fibonacciCallable;
    private SchedulerExample schedulerExample;
    private SynchronizeTest synchronizeTest;
    private PipeToCommunicate pipeToCommunicate;

    public Controller() {
        this.pingPong = new PingPong();
        this.fibonacciExecutors = new FibonacciExecutors();
        this.fibonacciNumbers = new FibonacciNumbers();
        this.fibonacciCallable = new FibonacciCallable();
        this.schedulerExample = new SchedulerExample();
        this.synchronizeTest = new SynchronizeTest();
        this.pipeToCommunicate = new PipeToCommunicate();
    }

    //task 1
    public void runPlayPingPong() {
        pingPong.playPingPong();
    }

    //task 2
    public void runFibonacciNumberCreation() {
        fibonacciNumbers.createNumberFibonacci();
    }

    //task 3
    public void runFibonacciExecutors() {
        fibonacciExecutors.createNumberFibonacci();
    }
    //task 4
    public void runSumsValuesOfAllFibonacciNumbers(){
        fibonacciCallable.sumsValuesOfAllFibonacciNumbers();
    }

    public void runSleepForRandomAmount(){
        schedulerExample.sleepForRandomAmount();
    }

    public void runSomeSynchronizedMethods(){
        synchronizeTest.someSynchronizedMethods();
    }

    public void runUsePipeStream(){
        pipeToCommunicate.usePipeStream();
    }
}
