package com.bubnii.view;

import com.bubnii.controller.Controller;
import com.bubnii.interfaces.Printable;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {

    private Controller controller = new Controller();
    private MessagePrinter print = new MessagePrinter();
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Scanner input;

    public View() {
        input = new Scanner(System.in);
        menu = new LinkedHashMap<>();
        menu.put("1", "\t1 - Show ping pong game");
        menu.put("2", "\t2 - Show Fibonacci numbers");
        menu.put("3", "\t3 - Show Fibonacci numbers of executors");
        menu.put("4", "\t4 - Show sums values of all Fibonacci numbers");
        menu.put("5", "\t5 - Show sleep for random amount");
        menu.put("6", "\t6 - Show some synchronized methods of \"this\" and \"object\"");
        menu.put("7", "\t7 - Show task use a pipe to communicate");
        menu.put("Q", "\tQ - exit");
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::showPlayPingPong);
        methodsMenu.put("2", this::showFibonacciNumberCreation);
        methodsMenu.put("3", this::showFibonacciExecutors);
        methodsMenu.put("4", this::showSumsValuesOfAllFibonacciNumbers);
        methodsMenu.put("5", this::showSleepForRandomAmount);
        methodsMenu.put("6", this::showSomeSynchronizedMethods);
        methodsMenu.put("7", this::showUsePipeStream);
    }

    private void showPlayPingPong() {
        controller.runPlayPingPong();
    }

    private void showFibonacciNumberCreation() {
        controller.runFibonacciNumberCreation();
    }

    private void showFibonacciExecutors() {
        controller.runFibonacciExecutors();
    }

    private void showSumsValuesOfAllFibonacciNumbers() {
        controller.runSumsValuesOfAllFibonacciNumbers();
    }

    private void showSleepForRandomAmount() {
        controller.runSleepForRandomAmount();
    }

    private void showSomeSynchronizedMethods() {
        controller.runSomeSynchronizedMethods();
    }

    private void showUsePipeStream(){
        controller.runUsePipeStream();
    }

    public void show() {
        String keyMenu;
        do {
            print.printMessage("------------------------------------------------------------------------------------------");
            outputMenu();
            print.printMessage("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            quitProgram(keyMenu);
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (true);
    }

    private void quitProgram(String keyMenu) {
        if (keyMenu.equals("Q")) {
            System.exit(0);
        }
    }

    private void outputMenu() {
        print.printMessage("MENU:");
        for (String str : menu.values()) {
            print.printMessage(str);
        }
    }
}
