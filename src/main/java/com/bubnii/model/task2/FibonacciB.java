package com.bubnii.model.task2;

import com.bubnii.view.MessagePrinter;

public class FibonacciB implements Runnable {

    private MessagePrinter print = new MessagePrinter();
    private int n ;

    public FibonacciB(int n) {
        this.n = n;
    }

    private int fib(int x) {
        if (x < 2) return 1;
        return fib(x - 2) + fib(x - 1);
    }

    public void run() {
        for (int i = 0; i < n; i++)
            print.printMessage(fib(i) + " ");
        print.printMessage("");
    }
}