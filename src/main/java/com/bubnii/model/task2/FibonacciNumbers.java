package com.bubnii.model.task2;

public class FibonacciNumbers {

    public void createNumberFibonacci() {
        Thread f1 = new Thread(new FibonacciA(15));
        Thread f2 = new Thread(new FibonacciB(15));
        Thread f3 = new Thread(new FibonacciC(15));
        f1.start();
        f2.start();
        f3.start();
    }
}
