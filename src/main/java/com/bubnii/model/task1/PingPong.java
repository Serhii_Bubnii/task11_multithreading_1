package com.bubnii.model.task1;

import com.bubnii.view.MessagePrinter;
import com.bubnii.view.View;

import java.time.LocalDateTime;

public class PingPong {

    private static final Object sync = new Object();
    private MessagePrinter print = new MessagePrinter();

    public void playPingPong() {
        Thread threadOne = getThreadOne();
        Thread threadTwo = getThreadTwo();

        print.printMessage(LocalDateTime.now());
        threadOne.start();
        threadTwo.start();
        try {
            threadOne.join();
            threadTwo.join();
        } catch (InterruptedException e) {
            print.printMessage(e.getStackTrace());
        }
        print.printMessage(LocalDateTime.now());
    }

    private Thread getThreadTwo() {
        return new Thread(() -> {
            Thread.currentThread().setName("Thread Two");
            synchronized (sync) {
                for (int i = 1; i <= 100; i++) {
                    sync.notify();
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        print.printMessage(e.getStackTrace());
                    }
                    print.printMessage("\nPong " + i);
                }
                print.printMessage("finish " + Thread.currentThread().getName());
            }
        });
    }

    private Thread getThreadOne() {
        return new Thread(() -> {
            Thread.currentThread().setName("Thread One");
            synchronized (sync) {
                for (int i = 1; i <= 100; i++) {
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        print.printMessage(e.getStackTrace());
                    }
                    print.printMessage("\n\tPing " + i);
                    sync.notify();
                }
                print.printMessage("finish " + Thread.currentThread().getName());
            }
        });
    }
}
