package com.bubnii.model.task6;

import com.bubnii.view.MessagePrinter;

public class SynchronizedOnThis {

    private MessagePrinter printer = new MessagePrinter();

    public void methodOne() {
        synchronized (this) {
            for (int i = 0; i < 5; i++) {
                printer.printMessage("this.methodOne");
                Thread.yield();
            }
        }
    }

    public void methodTwo() {
        synchronized (this) {
            for (int i = 0; i < 5; i++) {
                printer.printMessage("this.methodTwo");
                Thread.yield();
            }
        }
    }

    public void methodThree() {
        synchronized (this) {
            for (int i = 0; i < 5; i++) {
                printer.printMessage("this.methodThree");
                Thread.yield();
            }
        }
    }
}

