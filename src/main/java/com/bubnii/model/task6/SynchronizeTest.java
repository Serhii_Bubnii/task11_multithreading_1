package com.bubnii.model.task6;

public class SynchronizeTest {

    private final SynchronizedOnThis synchronizedOnThis =
            new SynchronizedOnThis();
    private final SynchronizedOnDifferentObjects synchronizedOnDifferentObjects =
            new SynchronizedOnDifferentObjects();

    public void someSynchronizedMethods() {
        new Thread(synchronizedOnThis::methodOne).start();
        new Thread(synchronizedOnThis::methodTwo).start();
        new Thread(synchronizedOnThis::methodThree).start();

        new Thread(() -> synchronizedOnDifferentObjects.methodOne()).start();
        new Thread(() -> synchronizedOnDifferentObjects.methodTwo()).start();
        new Thread(() -> synchronizedOnDifferentObjects.methodThree()).start();
    }
}
