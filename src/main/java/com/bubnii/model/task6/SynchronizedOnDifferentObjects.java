package com.bubnii.model.task6;

import com.bubnii.view.MessagePrinter;

public class SynchronizedOnDifferentObjects {
    private MessagePrinter printer = new MessagePrinter();

    public void methodOne() {
        synchronized (printer) {
            for (int i = 0; i < 5; i++) {
                printer.printMessage("Object.methodOne");
                Thread.yield();
            }
        }
    }

    public void methodTwo() {
        synchronized (printer) {
            for (int i = 0; i < 5; i++) {
                printer.printMessage("Object.methodTwo");
                Thread.yield();
            }
        }
    }

    public void methodThree() {
        synchronized (printer) {
            for (int i = 0; i < 5; i++) {
                printer.printMessage("Object.methodThree");
                Thread.yield();
            }
        }
    }
}
