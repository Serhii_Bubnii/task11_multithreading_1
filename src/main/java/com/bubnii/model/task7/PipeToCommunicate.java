package com.bubnii.model.task7;

import java.io.*;

public class PipeToCommunicate {

    private PipedInputStream pipedInput = new PipedInputStream();
    private PipedOutputStream pipedOutput = new PipedOutputStream();

    public void usePipeStream() {
        try {
            pipedInput.connect(pipedOutput);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Thread pipeWriter = new Thread(() -> {
            for (int i = 65; i < 91; i++) {
                try {
                    pipedOutput.write(i);
                    System.out.println(" Pipe output " + i);
                    Thread.sleep(500);
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread pipeReader = new Thread(() -> {
            for (int i = 65; i < 91; i++) {
                try {
                    System.out.println("Pipe reader " + (char) pipedInput.read());
                    Thread.sleep(1000);
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        pipeWriter.start();
        pipeReader.start();
        try {
            pipeWriter.join();
            pipeReader.join();
            pipedInput.close();
            pipedOutput.close();
        } catch (IOException |
                InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        PipeToCommunicate pipe = new PipeToCommunicate();
        pipe.usePipeStream();
    }
}
