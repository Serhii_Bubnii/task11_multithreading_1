package com.bubnii.model.task4;

import java.util.concurrent.Callable;

public class Fibonacci implements Callable<Integer> {

    private int n;

    public Fibonacci(int n) {
        this.n = n;
    }

    private int fib(int x) {
        if (x < 2) return 1;
        return fib(x - 2) + fib(x - 1);
    }

    @Override
    public Integer call() {
        int result = 0;
        for (int i = 0; i < n; i++)
            result += fib(i);
        return result;
    }
}
