package com.bubnii.model.task4;

import com.bubnii.view.MessagePrinter;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class FibonacciCallable {

    private MessagePrinter print = new MessagePrinter();

    public void sumsValuesOfAllFibonacciNumbers() {
        ExecutorService exec = Executors.newCachedThreadPool();
        ArrayList<Future<Integer>> results = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            results.add(exec.submit(new Fibonacci(i)));
        }
        for (Future<Integer> fs : results) {
            try {
                print.printMessage(fs.get());
            } catch (InterruptedException e) {
                print.printMessage(e);
                return;
            } catch (ExecutionException e) {
                print.printMessage(e);
            } finally {
                exec.shutdown();
            }
        }
    }
}
