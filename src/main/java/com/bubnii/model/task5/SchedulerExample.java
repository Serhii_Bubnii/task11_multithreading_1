package com.bubnii.model.task5;

import com.bubnii.view.MessagePrinter;

import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SchedulerExample implements Runnable {

    private Random rand = new Random();
    private Scanner input = new Scanner(System.in);
    private MessagePrinter printer = new MessagePrinter();

    @Override
    public void run() {
        try {
            int t = 1000 * rand.nextInt(10);
            TimeUnit.MILLISECONDS.sleep(t);
            printer.printMessage("Slept " + t / 1000 + " seconds");
            return;
        } catch (InterruptedException e) {
            printer.printMessage("Interrupted");
        }
    }

    public void sleepForRandomAmount() {
        printer.printMessage("Please enter number from 1 to 10: ");
        int value = input.nextInt();
        ExecutorService exec = Executors.newCachedThreadPool();
        for (int i = 0; i < value; i++) {
            exec.execute(new SchedulerExample());
        }
        exec.shutdown();
    }
}
