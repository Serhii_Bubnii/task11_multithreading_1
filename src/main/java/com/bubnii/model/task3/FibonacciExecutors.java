package com.bubnii.model.task3;

import com.bubnii.model.task2.FibonacciA;
import com.bubnii.model.task2.FibonacciB;
import com.bubnii.model.task2.FibonacciC;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FibonacciExecutors {

    public void createNumberFibonacci(){
        ExecutorService serviceOne = Executors.newCachedThreadPool();
        serviceOne.execute(new FibonacciA(10));
        serviceOne.execute(new FibonacciB(10));
        serviceOne.execute(new FibonacciC(10));
        serviceOne.shutdown();
        ExecutorService serviceTwo = Executors.newFixedThreadPool(3);
        serviceTwo.execute(new FibonacciA(10));
        serviceTwo.execute(new FibonacciB(10));
        serviceTwo.execute(new FibonacciC(10));
        serviceTwo.shutdown();
        ExecutorService serviceThree = Executors.newSingleThreadExecutor();
        serviceThree.execute(new FibonacciA(10));
        serviceThree.execute(new FibonacciB(10));
        serviceThree.execute(new FibonacciC(10));
        serviceThree.shutdown();
    }
}
